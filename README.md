# K-means经典代码案例

K-means 是一种常用的聚类算法，用于将数据集划分为 K 个簇。以下是一个经典的 K-means 算法的 Python 实现案例，使用 `numpy` 和 `matplotlib` 库。

### 1. 导入必要的库

```python
import numpy as np
import matplotlib.pyplot as plt
```

### 2. 生成随机数据

```python
# 生成随机数据
np.random.seed(42)
X = np.random.rand(100, 2)  # 100个二维数据点
```

### 3. 定义 K-means 算法

```python
def kmeans(X, K, max_iters=100):
    # 随机初始化 K 个中心点
    centroids = X[np.random.choice(X.shape[0], K, replace=False)]
    
    for i in range(max_iters):
        # 计算每个数据点到中心点的距离
        distances = np.sqrt(((X - centroids[:, np.newaxis])**2).sum(axis=2))
        
        # 分配每个数据点到最近的中心点
        labels = np.argmin(distances, axis=0)
        
        # 更新中心点
        new_centroids = np.array([X[labels == k].mean(axis=0) for k in range(K)])
        
        # 如果中心点不再变化，停止迭代
        if np.all(centroids == new_centroids):
            break
        
        centroids = new_centroids
    
    return labels, centroids
```

### 4. 运行 K-means 算法

```python
K = 3  # 假设我们要将数据分为3个簇
labels, centroids = kmeans(X, K)
```

### 5. 可视化结果

```python
# 绘制数据点和簇中心
plt.scatter(X[:, 0], X[:, 1], c=labels, cmap='viridis', s=50)
plt.scatter(centroids[:, 0], centroids[:, 1], c='red', s=200, alpha=0.75, marker='X')
plt.title('K-means Clustering')
plt.show()
```

### 6. 完整代码

```python
import numpy as np
import matplotlib.pyplot as plt

# 生成随机数据
np.random.seed(42)
X = np.random.rand(100, 2)  # 100个二维数据点

def kmeans(X, K, max_iters=100):
    # 随机初始化 K 个中心点
    centroids = X[np.random.choice(X.shape[0], K, replace=False)]
    
    for i in range(max_iters):
        # 计算每个数据点到中心点的距离
        distances = np.sqrt(((X - centroids[:, np.newaxis])**2).sum(axis=2))
        
        # 分配每个数据点到最近的中心点
        labels = np.argmin(distances, axis=0)
        
        # 更新中心点
        new_centroids = np.array([X[labels == k].mean(axis=0) for k in range(K)])
        
        # 如果中心点不再变化，停止迭代
        if np.all(centroids == new_centroids):
            break
        
        centroids = new_centroids
    
    return labels, centroids

# 运行 K-means 算法
K = 3  # 假设我们要将数据分为3个簇
labels, centroids = kmeans(X, K)

# 绘制数据点和簇中心
plt.scatter(X[:, 0], X[:, 1], c=labels, cmap='viridis', s=50)
plt.scatter(centroids[:, 0], centroids[:, 1], c='red', s=200, alpha=0.75, marker='X')
plt.title('K-means Clustering')
plt.show()
```

### 7. 运行结果

运行上述代码后，你将看到一个二维散点图，其中数据点被分为 K 个簇，每个簇的中心点用红色标记。

### 8. 解释

- `X` 是一个包含 100 个二维数据点的数组。
- `kmeans` 函数实现了 K-means 算法，返回每个数据点的标签（即所属簇）和最终的簇中心。
- `plt.scatter` 用于绘制数据点和簇中心。

这个案例展示了如何使用 Python 实现 K-means 算法，并将其应用于简单的二维数据集。你可以根据需要调整数据集、簇的数量 `K` 以及其他参数。